#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

#define ssid "Sonic Soul Surfer"
#define password "XYZ"
#define mqtt_server "10.10.10.10"
#define mqtt_port 1883
#define topic "button1"
#define topic_content "button_pressed"

IPAddress ip( 10, 10, 10, 28 );
IPAddress gateway( 10, 10, 10, 1 );
IPAddress subnet( 255, 255, 255, 0 );
IPAddress dns( 8, 8, 8, 8 );

uint8_t bssid[] = {0x54, 0xB8, 0x0A, 0xAA, etc, etc};

WiFiClient espClient;
PubSubClient client(espClient);

void setup(void)
{
    WiFi.persistent( false );
    WiFi.config( ip, gateway, subnet, dns);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password, 10, bssid, true);

    while (WiFi.status() != WL_CONNECTED) {
      delay(10);
    }

    while (!client.connected()) {
    client.setServer(mqtt_server, mqtt_port);
    client.connect("button1", "", "");
    }
  
    client.publish(topic, topic_content, true);
    client.disconnect();
  
    WiFi.disconnect( true );
}

void loop(void)
{
  ESP.deepSleep(0);
}